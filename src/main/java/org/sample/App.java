package org.sample;

import java.io.*;
import java.nio.file.*;
import java.sql.*;
import java.util.concurrent.*;

import javax.xml.parsers.*;

import org.apache.http.*;
import org.elasticsearch.*;
import org.elasticsearch.action.get.*;
import org.elasticsearch.action.search.*;
import org.elasticsearch.client.*;
import org.elasticsearch.common.unit.*;
import org.elasticsearch.index.query.*;
import org.elasticsearch.rest.*;
import org.elasticsearch.search.*;
import org.elasticsearch.search.builder.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.*;
import org.w3c.dom.*;

@State(Scope.Benchmark)
public class App {

	private String POSTGRESQL_USER;
	private String POSTGRESQL_PASSWORD;
	private String POSTGRESQL_CONNECTION_URL;
	private String ELASTICSEARCH_HOSTNAME;
	private Integer ELASTICSEARCH_PORT;
	private String ELASTICSEARCH_ESQUEMA;
	private String ELASTICSEARCH_INDICE;

	private java.sql.Connection conexionPsql;

	private RestHighLevelClient elasticClient;

	private SearchRequest elasticRequest;

	private java.sql.ResultSet psqlResultSet;

	private java.sql.Statement psqlStatement;

	private String psqlQuery = "SELECT * FROM title_basics WHERE original_title LIKE '%Boxing%' ";

	public void initPsql() {
		conexionPsql = null;
		psqlStatement = null;
		psqlResultSet = null;
		try {
			conexionPsql = java.sql.DriverManager.getConnection(POSTGRESQL_CONNECTION_URL, POSTGRESQL_USER,
					POSTGRESQL_PASSWORD);
			//System.out.println("Connected to the PostgreSQL server successfully.");

			psqlStatement = conexionPsql.createStatement();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	public void connectElastic() {

		RestHighLevelClient client = null;
		try {
			client = new RestHighLevelClient(RestClient
					.builder(new HttpHost(ELASTICSEARCH_HOSTNAME, ELASTICSEARCH_PORT, ELASTICSEARCH_ESQUEMA)));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		elasticClient = client;
	}

	@TearDown(Level.Trial)
	public void doTearDown() {
		System.out.println("Do TearDown");
		try {
			if (elasticClient != null) {
				elasticClient.close();
			}
			if (conexionPsql != null) {
				conexionPsql.close();
			}
			if (psqlResultSet != null) {
				psqlResultSet.close();
			}
			if (psqlStatement != null) {
				psqlStatement.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void elasticRequest() {

		SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
		sourceBuilder.query(QueryBuilders.matchQuery("original_title", "Boxing"));
		sourceBuilder.size(1000);

		SearchRequest searchRequest = new SearchRequest();
		searchRequest.indices(ELASTICSEARCH_INDICE);
		searchRequest.source(sourceBuilder);

		elasticRequest = searchRequest;
	}

	public void init_configuracion() {
		try {
			Path dir = Paths.get("").toAbsolutePath();

			//System.out.println(dir.toString());

			File file = new File(dir + File.separator + "configuracion.xml");

			BufferedReader br = new BufferedReader(new FileReader(file));

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file);

			POSTGRESQL_USER = doc.getDocumentElement().getElementsByTagName("POSTGRESQL_USER").item(0).getTextContent();
			POSTGRESQL_PASSWORD = doc.getDocumentElement().getElementsByTagName("POSTGRESQL_PASSWORD").item(0)
					.getTextContent();
			POSTGRESQL_CONNECTION_URL = doc.getDocumentElement().getElementsByTagName("POSTGRESQL_CONNECTION_URL")
					.item(0).getTextContent();
			ELASTICSEARCH_HOSTNAME = doc.getDocumentElement().getElementsByTagName("ELASTICSEARCH_HOSTNAME").item(0)
					.getTextContent();
			ELASTICSEARCH_PORT = Integer.parseInt(
					doc.getDocumentElement().getElementsByTagName("ELASTICSEARCH_PORT").item(0).getTextContent());
			ELASTICSEARCH_ESQUEMA = doc.getDocumentElement().getElementsByTagName("ELASTICSEARCH_ESQUEMA").item(0)
					.getTextContent();
			ELASTICSEARCH_INDICE = doc.getDocumentElement().getElementsByTagName("ELASTICSEARCH_INDICE").item(0)
					.getTextContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Setup(Level.Trial)
	public void setup() {
		System.out.println("Do Setup");
		init_configuracion();
		initPsql();
		connectElastic();
		elasticRequest();
	}

	public static void main(String[] args) throws Exception {

	}

	public SearchResponse elastiSearchResponse() throws IOException {
		return elasticClient.search(elasticRequest, RequestOptions.DEFAULT);
	}

	public ResultSet psqlResponse() throws SQLException {
		return psqlStatement.executeQuery(psqlQuery);
	}

	@Benchmark
	@BenchmarkMode(Mode.AverageTime)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public void testElasticMethod(Blackhole blackhole) throws IOException {
		// blackhole.consume(elastiSearchResponse());
		blackhole.consume(elasticClient.search(elasticRequest, RequestOptions.DEFAULT));
	}

	@Benchmark
	@BenchmarkMode(Mode.AverageTime)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public void testPsqlMethod(Blackhole blackhole) throws SQLException {
		// blackhole.consume(psqlResponse());
		blackhole.consume(psqlStatement.executeQuery(psqlQuery));
	}

	public String getPOSTGRESQL_USER() {
		return POSTGRESQL_USER;
	}

	public void setPOSTGRESQL_USER(String pOSTGRESQL_USER) {
		POSTGRESQL_USER = pOSTGRESQL_USER;
	}

	public String getPOSTGRESQL_PASSWORD() {
		return POSTGRESQL_PASSWORD;
	}

	public void setPOSTGRESQL_PASSWORD(String pOSTGRESQL_PASSWORD) {
		POSTGRESQL_PASSWORD = pOSTGRESQL_PASSWORD;
	}

	public String getPOSTGRESQL_CONNECTION_URL() {
		return POSTGRESQL_CONNECTION_URL;
	}

	public void setPOSTGRESQL_CONNECTION_URL(String pOSTGRESQL_CONNECTION_URL) {
		POSTGRESQL_CONNECTION_URL = pOSTGRESQL_CONNECTION_URL;
	}

	public String getELASTICSEARCH_HOSTNAME() {
		return ELASTICSEARCH_HOSTNAME;
	}

	public void setELASTICSEARCH_HOSTNAME(String eLASTICSEARCH_HOSTNAME) {
		ELASTICSEARCH_HOSTNAME = eLASTICSEARCH_HOSTNAME;
	}

	public Integer getELASTICSEARCH_PORT() {
		return ELASTICSEARCH_PORT;
	}

	public void setELASTICSEARCH_PORT(Integer eLASTICSEARCH_PORT) {
		ELASTICSEARCH_PORT = eLASTICSEARCH_PORT;
	}

	public String getELASTICSEARCH_ESQUEMA() {
		return ELASTICSEARCH_ESQUEMA;
	}

	public void setELASTICSEARCH_ESQUEMA(String eLASTICSEARCH_ESQUEMA) {
		ELASTICSEARCH_ESQUEMA = eLASTICSEARCH_ESQUEMA;
	}

	public String getELASTICSEARCH_INDICE() {
		return ELASTICSEARCH_INDICE;
	}

	public void setELASTICSEARCH_INDICE(String eLASTICSEARCH_INDICE) {
		ELASTICSEARCH_INDICE = eLASTICSEARCH_INDICE;
	}

	public java.sql.Connection getConexionPsql() {
		return conexionPsql;
	}

	public void setConexionPsql(java.sql.Connection conexionPsql) {
		this.conexionPsql = conexionPsql;
	}

	public RestHighLevelClient getElasticClient() {
		return elasticClient;
	}

	public void setElasticClient(RestHighLevelClient elasticClient) {
		this.elasticClient = elasticClient;
	}

	public SearchRequest getElasticRequest() {
		return elasticRequest;
	}

	public void setElasticRequest(SearchRequest elasticRequest) {
		this.elasticRequest = elasticRequest;
	}

	public java.sql.ResultSet getPsqlResultSet() {
		return psqlResultSet;
	}

	public void setPsqlResultSet(java.sql.ResultSet psqlResultSet) {
		this.psqlResultSet = psqlResultSet;
	}

	public java.sql.Statement getPsqlStatement() {
		return psqlStatement;
	}

	public void setPsqlStatement(java.sql.Statement psqlStatement) {
		this.psqlStatement = psqlStatement;
	}

	public String getPsqlQuery() {
		return psqlQuery;
	}

	public void setPsqlQuery(String psqlQuery) {
		this.psqlQuery = psqlQuery;
	}

}